//var request = require("request");
//var fs = require("fs");
//var $ = require("jquery");
import $ from 'jquery';
import conf from '../config/conf.json';
//var conf =  JSON.parse(fs.readFileSync("../config/conf.json"));


class RestAdmin {
    constructor(props) {
        this.hosturl = conf.servHost;//"http://localhost:50000";//conf.servHost;
       // this.hosturl = "https://bluezinc.cl:50000";//conf.servHost;        
        //this._sessToken = null;      
    }
    
    
    
    callService(service,par,callback){   
        console.log("init callService");  
        par = par||{}; 
        if(typeof par === "function"){
            callback = par;
            par = {};
        }
        
        $.ajax({
            type: par.method||"GET",
            dataType: 'json',
            url: this.hosturl+service,
            xhrFields: {
                         withCredentials: true
                        },                       
            data: par.body||"",
            crossDomain: true,  
            beforeSend: function (xhr)  { 
                //xhr.setRequestHeader('Authorization', this.sessToken); 
            },     
            success: function (output, status, xhr) {
                if(callback)
                    callback(output);
            },
            error: function (xhr, txtStat, errThrown) {
                    var err = {};
                    err.xhr = xhr;
                    err.txtStat = txtStat;
                    err.errThrown = errThrown;
                    console.log(err);
                if(callback)
                    callback(err);
            }
        });
    }
  }
  export default (RestAdmin);