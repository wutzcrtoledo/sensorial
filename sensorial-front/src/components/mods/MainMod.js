import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import CameraTable from '../custComp/CameraTable';
import moment from 'moment-timezone';
import _ from 'underscore';
import Delegate from '../../lib/Delegate';

const styles = theme => ({
    mainContainer: {
        //paddingTop: 20,
        width: '100%',
    },
    camTableCol: {
        width: 400,
        height: 400,
        position: 'relative'
    }
    
  });

function GetCamera(props){
    let modifiedEvents = _.map(props.evens, (nod) => {
         return {event_id:nod.event_id, 
                 time: moment(nod.when).tz("America/Santiago").format("HH:mm:ss"), 
                 status: nod.status||'-'};
    });
    return (<Grid item xs={12} md={4} >
                <CameraTable cameraName={props.camName} camMeta={props.camMeta} events={modifiedEvents}  />
            </Grid>);
}

function GetGrid(props){
    return(<Grid container spacing={16} >
                {props.camList.map((object, i) => <GetCamera key={i} camName={object[0]} camMeta={props.sensMeta?props.sensMeta[object[0]]:{}} evens={object[1]} />)}
           </Grid>)    
};
/**
 * Cameras === Sensors
 */
class MainMod extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {      
      selectedDate: null,   
      cameras : {},
      camerasMeta:null
    };
    this.deleg = new Delegate();
  }

  refreshSensorList = () => {
    let mainObj = this;
    if(!this.state.camerasMeta){
        this.deleg.getSensors(_sen => {
            mainObj.setState({camerasMeta: _sen.items});
        });
    }
    let newDate = this.state.selectedDate;
    console.log(newDate.format("YYYY-MM-DD"));
    this.props.parentPipe().switchLoading.ON();
    this.deleg.getEvents(newDate.format("YYYY-MM-DD"), _res =>{
        this.props.parentPipe().switchLoading.OFF();
        console.log(_res);
        if(_res.OK)
            this.setState({cameras: _res.items});
    });
  }

  checkDateChange = () => {
    if(!this.state.selectedDate || this.state.selectedDate.format("DD-MM-YYYY") !== this.props.parentPipe().selectedDate.format("DD-MM-YYYY")){
        this.setState({selectedDate: this.props.parentPipe().selectedDate, cameras:{}});
       // this.state.selectedDate = this.props.parentPipe().selectedDate;
        setTimeout(()=>{
            this.refreshSensorList();
        },300);
    }    
  }

  render() {
    if(this.props.parentPipe().sessActive)
        this.checkDateChange();
    const { classes, theme } = this.props; 
    let gridLst = _.chunk(_.pairs(this.state.cameras), 3); 
    return (
       <div>   
            {gridLst.map((object, i) => <GetGrid key={i} camList={object} sensMeta={this.state.camerasMeta} />)}   
       </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(MainMod);