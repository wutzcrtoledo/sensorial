import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import Done from '@material-ui/icons/Done';
import Cancel from '@material-ui/icons/Cancel';


import Delegate from '../lib/Delegate';

const styles = theme => ({
     root: {
         padding: 30
     }    
});


class ResetPassForm extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        currPass:"",
        newPass: "",
        newPassRepeat: ""      
      };
      this.delObj = new Delegate();
      this.handleChange = this.handleChange.bind(this);
      this.refreshInputs = this.refreshInputs.bind(this);
    }
  
    handleChange(ev){
        ev.preventDefault();
        let compIns = this;
        //console.log(this.state);
        if(this.state.newPass !== this.state.newPassRepeat){
            this.props.showDialogMessage("Los password a repetir no sin iguales","error");
        }
        else{
            compIns.delObj.changePassword(this.state.currPass,this.state.newPass , _res => {
               if(_res.OK){
                     this.props.showDialogMessage("El password fue cambiado","success"); 
                     this.props.onReady(); 
                }
                else
                     this.props.showDialogMessage("Error al cambiar password. Asegurese que el password actual es correcto e intente de nuevo","error"); 
            });
        }
    }

    refreshInputs() {
        this.setState({
            currPass:document.getElementById("currPass").value,
            newPass: document.getElementById("newPass").value,
            newPassRepeat: document.getElementById("newPassRepeat").value
        });
    }

  render() {
       const { classes } = this.props;
       return(
           <Dialog open={this.props.open} >
           <Paper className={classes.root} elevation={1}>
                <Typography variant="h5" component="h3">
                    Cambiar Password
                </Typography>
                <form className={classes.form} onSubmit={this.handleChange}>
                    <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="currPass">Password Actual</InputLabel>
                        <Input id="currPass" 
                               type="password"
                               name="currPass" 
                               defaultValue={this.state.currPass} 
                               onKeyUp={this.refreshInputs}  autoFocus />
                    </FormControl>
                    <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="newPass">Nuevo Password</InputLabel>
                        <Input
                            name="newPass"
                            type="password"
                            id="newPass" 
                            defaultValue={this.state.newPass}
                            onKeyUp={this.refreshInputs}
                        />
                    </FormControl>  
                    <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="newPass">Repetir Nuevo Password</InputLabel>
                        <Input
                            name="newPassRepeat"
                            type="password"
                            id="newPassRepeat" 
                            defaultValue={this.state.newPassRepeat}
                            onKeyUp={this.refreshInputs}
                        />
                    </FormControl> 
                    <Typography variant="h5" component="h3">
                        
                     </Typography>    
                     <div style={{textAlign:"center"}}>
                     <Tooltip title="Cancelar">                         
                        <Button centerRipple variant="fab" type="button" onClick={this.props.onReady} color='default' aria-label="Cancelar">
                        <Cancel />
                        </Button>            
                    </Tooltip> 
                     <Tooltip title="Cambiar Password">                         
                        <Button centerRipple variant="fab" type="submit" color='primary' aria-label="Cambiar">
                        <Done />
                        </Button>            
                    </Tooltip> 
                    </div>
                </form>
            </Paper>                
           </Dialog>
       );
  }
}

export default withStyles(styles, { withTheme: true })(ResetPassForm);