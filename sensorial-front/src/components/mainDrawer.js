import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import DatePicker from 'material-ui-pickers/DatePicker/DatePickerInline';
import Button from '@material-ui/core/Button';
import InsertInvitation from '@material-ui/icons/InsertInvitation';
import PowerSettingsNew from '@material-ui/icons/PowerSettingsNew';
import DateRange from '@material-ui/icons/DateRange';
import EditAttributes from '@material-ui/icons/EditAttributes';
import Tooltip from '@material-ui/core/Tooltip';
import Slide from '@material-ui/core/Slide';
import MuiPickersUtilsProvider from "material-ui-pickers/utils/MuiPickersUtilsProvider";
import MomentUtils from "material-ui-pickers/utils/moment-utils";
import CircularProgress from '@material-ui/core/CircularProgress';
import Alert from 'react-s-alert';
import moment from 'moment';
import 'moment/locale/es';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/jelly.css';
import MainModsController from './modsController';
import LoginForm from './LoginForm';
import ResetPassForm from './ResetPassForm';
import Delegate from '../lib/Delegate';

let $ = require("jquery");
moment.locale("es");

const styles = theme => ({
  root: {
    flexGrow: 1,
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    width: '100%',
  },
  appBar: {
    position: 'absolute',
    width: '100%'
  },  
  toolbar: theme.mixins.toolbar,  
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    overflowY: 'auto',
    position: 'relative'
  },
  fabButton: {
    position: 'absolute',
    top: 30,
    left: 0,
    right: 0,
    //margin: '0 auto',
    paddingLeft: '45%'
  },
  fabSessButton:{
    position: 'relative',
    top: 30,
    marginLeft : 'auto'
  }, 
  loadingCont:{
    paddingTop: "30px",
    paddingLeft: "45%",
    paddingRight: "auto"
  }
});


function SessionMenuOptions(props){
  return (<div className={props.class}>
          <Tooltip title="Cambiar Contraseña">
            <Button variant="fab" mini={true} onClick={props.handleChangePassword} color="default" aria-label="Cambiar Contraseña" >
              <EditAttributes />
            </Button>
          </Tooltip>
          <Tooltip title="Salir">
            <Button variant="fab" color="secondary" mini={true} onClick={props.handleLogout} aria-label="Logout">
              <PowerSettingsNew />
            </Button>
          </Tooltip>
          </div>);
}

class MainDrawer extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loggedUser : null,
      resetPass: false,
      mobileOpen: false,
      module: "mainMod",
      showModule: true ,
      selectedDate: moment(),
      loading : false, 
      showMainCalendar: true
    };     
    this.deleg = new Delegate();
    this.handleLoad = this.handleLoad.bind(this); 
    this.checkSession = this.checkSession.bind(this); 
    this.switchLoading = {ON: this.switchLoadingON.bind(this), OFF: this.switchLoadingOFF.bind(this)}
  }  
  

  componentDidMount() {
    window.addEventListener('load', this.handleLoad);
  }

  checkSession(){
    console.log("init checkSession");
    if(sessionStorage.getItem("loginData")){
        this.sessionReady(JSON.parse(sessionStorage.getItem("loginData")));
    }
  }

  handleLoad() {
   // console.log($);
    console.log($(window).height());
    $("main").height($(window).height()-65);
    this.checkSession();
  } 

  sessionReady = (_loginData) => {
    this.setState({ loggedUser: _loginData.user });
    sessionStorage.setItem("loginData", JSON.stringify(_loginData));
  };
  
  passChangeReady = (passChanged) => {
     this.setState({resetPass:false});
  };

  handleChangePassword = () => {
      this.setState({resetPass:true});
  };
  handleLogout = () => {
    let compIns = this;
    compIns.deleg.logOut( () => {
      compIns.setState({ loggedUser: null });
      sessionStorage.removeItem("loginData");
    });
  };

  switchLoadingON(){
      this.setState({loading:true});
  };

  switchLoadingOFF(){
      this.setState({loading:false});
  }

  switchToModule = (modName) =>{
    let mainD = this;
    mainD.setState({showModule: false });
    setTimeout(()=>{
      mainD.setState({module:modName, showModule: true});
    },500);    
    console.log(modName);
  }

  handleDrawerToggle = () => {
    this.setState({mobileOpen: !this.state.mobileOpen});
  }

  handleDateChange = (date) => {
    console.log("Changing Date");
    this.setState({ selectedDate: date });
    //this.switchToModule("mainMod");
    //$("#mainModPipeDateSelected").val(date.format("YYYY-MM-DD"));
    //this.refreshSensorList(date);
  }

  showDialogMessage = (msg, type, callback) => {
      if(typeof type === "function"){
        callback = type;
        type = null;
      }
      type = type || "success";
      let options = {
        position: 'top-right',
        effect: 'jelly',
        onShow: function () {
            if(callback)
                callback();
        },
        beep: false,
        timeout: 3000,
        offset: 100
      };
      switch (type) {
        case 'info':
          Alert.info(msg, options);
          break;
        case 'success':
          Alert.success(msg, options);
          break;
        case 'warning':
          Alert.warning(msg, options);
          break;
        case 'error':
           Alert.error(msg, options);
          break;
        default:  
          Alert.info(msg, options);
      }      
  };

  openPicker = (e) => {
    // do not pass Event for default pickers
    this.picker.open(e);
    if(this.state.module !== "mainMod"){
        this.switchToModule("mainMod");
        this.setState({showMainCalendar:true});
    }
  };

  openDateRange = () => {
     this.setState({showMainCalendar:false});
     this.switchToModule("dateRange");
  };

  parentPipe = () => {
    return ({selectedDate: this.state.selectedDate,
             sessActive: this.state.loggedUser?true:false,
             switchLoading: this.switchLoading}
            );
  };


  render() {
    const { classes, theme } = this.props;   

    return (
      <div className={classes.root}>
        <AppBar className={classes.appBar} position="sticky">
        <Toolbar>     
          <div className={classes.fabButton}>   
              <Tooltip title="Seleccionar Fecha">
                <Button variant="fab" color="primary" onClick={this.openPicker} aria-label="Cambiar Fecha" >
                  <InsertInvitation />
                </Button>            
              </Tooltip>
              <Tooltip title="Reporte Por Rango de Fecha">
                <Button variant="fab" color="default" onClick={this.openDateRange} aria-label="Rango de Fecha">
                  <DateRange />
                </Button>            
              </Tooltip>
          </div>
          <Typography variant="h6" color="inherit" noWrap>
              Bienvenido {this.state.loggedUser?this.state.loggedUser.fullName:"Invitado"}
          </Typography>
          <SessionMenuOptions class={classes.fabSessButton} handleLogout={this.handleLogout} handleChangePassword={this.handleChangePassword} /> 
        </Toolbar>
        </AppBar> 
        <main className={classes.content}>
            <div className={classes.toolbar} />
            <MuiPickersUtilsProvider utils={MomentUtils}>
                <div className={classes.picker}>
                  <DatePicker
                    disableFuture={true} 
                    style={{display:this.state.showMainCalendar?'block':'none'}} 
                    clearable
                    ref={(node) => { this.picker = node; }}
                    label="Fecha"
                    format="DD-MM-YYYY" 
                    fullWidth 
                    value={this.state.selectedDate}
                    onChange={this.handleDateChange}
                  />
                </div>
            </MuiPickersUtilsProvider>
            <Slide direction="left" in={this.state.showModule && this.state.loggedUser?true:false} mountOnEnter unmountOnExit>
              <MainModsController mod={this.state.module} parentPipe={this.parentPipe}  />
            </Slide>  
            <Slide direction="left" className={classes.loadingCont} in={this.state.loading} mountOnEnter unmountOnExit>
               <CircularProgress size={150} thickness={2.6}  />
            </Slide>           
          
        </main>
        <LoginForm open={!this.state.loggedUser} onReady={this.sessionReady} showDialogMessage={this.showDialogMessage}/>
        <ResetPassForm open={this.state.resetPass} onReady={this.passChangeReady} showDialogMessage={this.showDialogMessage}/>   
        <Alert className={classes.dialMessClass} stack={{limit: 3}} />     
      </div>
    );
  }
}

MainDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(MainDrawer);
