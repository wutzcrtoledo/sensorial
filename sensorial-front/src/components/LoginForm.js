import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import Done from '@material-ui/icons/Done';

import Delegate from '../lib/Delegate';

const styles = theme => ({
     root: {
         padding: 30
     }    
});


class LoginForm extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        userIdEmail:"",
        password: "",
        logged: false
      
      };
      this.delObj = new Delegate();
      this.handleLogin = this.handleLogin.bind(this);
      this.refreshInputs = this.refreshInputs.bind(this);
    }
  
    handleLogin(ev){
        ev.preventDefault()
        let compIns = this;
        compIns.delObj.login({"userId": this.state.userIdEmail,"password":this.state.password}, _res => {
            compIns.setState({logged:_res.logged});
            if(_res.logged){
               console.log("We are done");
               this.props.showDialogMessage("OK!","success");
               compIns.props.onReady(_res);
              //compIns.setState({ loggedUser: _res.user });
             }
             else{
                this.props.showDialogMessage("OH! Algo pasó!. Revise su usuario y contraseña e intente de nuevo.\nPara más información contacte al Administrador. ","error");
             }
        });
    }

    refreshInputs() {
        this.setState({
            userIdEmail:document.getElementById("email").value,
            password: document.getElementById("password").value
        });
    }

  render() {
        const { classes } = this.props;
       return(
           <Dialog open={this.props.open} >
           <Paper className={classes.root} elevation={1}>
                <Typography variant="h5" component="h3">
                    Sensorial.
                </Typography>
                <form className={classes.form} onSubmit={this.handleLogin}>
                    <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="email">Email o UserId</InputLabel>
                        <Input id="email" name="email" defaultValue={this.state.userIdEmail} onKeyUp={this.refreshInputs}  autoFocus />
                    </FormControl>
                    <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="password">Password</InputLabel>
                        <Input
                            name="password"
                            type="password"
                            id="password" 
                            defaultValue={this.state.password}
                            onKeyUp={this.refreshInputs}
                            autoComplete="current-password"
                        />
                    </FormControl>  
                    <Typography variant="h5" component="h3">
                        
                     </Typography>    
                     <div style={{textAlign:"center"}}>
                     <Tooltip title="Entrar">                         
                        <Button centerRipple variant="fab" type="submit" color='primary' onClick={this.openPicker} aria-label="Entrar">
                        <Done />
                        </Button>            
                    </Tooltip> 
                    </div>
                </form>
            </Paper>                
           </Dialog>
       );
  }
}

export default withStyles(styles, { withTheme: true })(LoginForm);