const _ = require("underscore");
const crypto = require("crypto");
const daoObj = require("../dao/DB");

"use strict";
var UserActBO = {
    logger : null
};
var UserActBOFnc = function(_logger) {UserActBO.logger = _logger;};
//Private

//Public
UserActBOFnc.prototype = UserActBO;

UserActBO.updatePassword = (_user, _currPass,_newPass, callback) =>{
    console.info(_user, _currPass,_newPass);
    daoObj.tbSeUser.findOne({where: {
        userId: _user.userId,
    }})
    .then(user => {
        console.log(user);
        var allOK = user && user.password === crypto.createHash('md5').update(_currPass).digest('hex');
        if(!allOK){
            callback({OK:false,msg:"wrong_curr_password"});
            return ;
        }
        user.password = crypto.createHash('md5').update(_newPass).digest('hex');
        daoObj.tbSeUser.update(_.pick(user,"password"), {where: {
                                        userId: user.userId,
                                     }})
        .then(_res => {
            console.log(_res);
            callback({OK:true});
            return ;
        })
        .catch(_err => {
            console.log(_err);
            callback({OK:false,msg:"internal"});
            return ;
        });
    });
};
  
module.exports = UserActBOFnc;