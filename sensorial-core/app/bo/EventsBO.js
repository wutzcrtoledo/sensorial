const evDao = require("../dao/EventsDAO");
const moment = require("moment-timezone");
const _ = require("underscore");

"use strict";
var EventsBO = {
    logger : null
};
var EventsBOFnc = function(_logger) {EventsBO.logger = _logger;};
//Private

//Public
EventsBOFnc.prototype = EventsBO;

EventsBO.getAllEventsForDate = (_user, _sensors,_date, callback) =>{
    _date = (_date && _date !== "")?moment(_date, "YYYY-MM-DD"):moment();
 //   console.log(_user.userId, _date.format("HH:mm:ss"));
     evDao.getEventsByDate(_user.userId, _date.format("YYYY-MM-DD") , _events => {
         var evBySen = _.groupBy(_events,"sensor_name");
          for(var i in _sensors){
            _sensors[i] =  evBySen[i]||[];
          }

	  for(var k in _sensors){
		let evs = _sensors[k];
	  }
          callback(_sensors);
     });
};

EventsBO.getSensorsForUser = (_user, callback) => {
    evDao.getSensorsForUser(_user.userId, _sens =>{
        callback(_.indexBy(_sens,"sensor_name"));
    });
};
  
module.exports = EventsBOFnc;
