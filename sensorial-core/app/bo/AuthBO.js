const _ = require("underscore");
const crypto = require("crypto");
const userDao = require("../dao/UsersDAO");

"use strict";
var AuthBO = {};
var AuthBOFnc = function(_sess, _logger) {
    AuthBO.logger = _logger;
    AuthBO.sess = _sess;
};
AuthBOFnc.prototype = AuthBO;

AuthBO.login = (_user, callback) => {
    var loggedin = true;
    userDao.getUser(_user, user => {
        loggedin = user && user.password === crypto.createHash('md5').update(_user.password).digest('hex');
        if(loggedin){
            //AuthBO.sess.cookie.maxAge = 5*60*1000; //5 Min Sec
            //AuthBO.sess.cookie.rolling = true;
            AuthBO.sess.user = {userId: user.userId, fullName: user.fullName, email:user.email};
            //AuthBO.sess.user.token = crypto.randomBytes(20).toString('hex'); No Needed for now
            //AuthBO.sess.user = _.omit(AuthBO.sess.user,"password");
            callback({logged : loggedin,user:AuthBO.sess.user});
            return ;
        }
        callback({logged : false,msg:"Not Able to Log in"});
    });   
};

AuthBO.refreshAccess_OLD = (_token) => {
     if(AuthBO.sess.user.token === _token){
        AuthBO.sess.user.token = crypto.randomBytes(20).toString('hex');
        return AuthBO.sess.user.token;
     }
     return false;
};

AuthBO.refreshSession = () => {
 //   let tmpUser = AuthBO.sess.user;
  //  AuthBO.sess.cookie.maxAge = 5*60*1000;   
   // AuthBO.sess = tmpUser;
};

AuthBO.logOut = () => {
   // AuthBO.sess.cookie.maxAge = 0;
    AuthBO.sess.user = null;
};

AuthBO.hasAccess_FINAL = (_token) => {
    return AuthBO.sess.user && AuthBO.sess.user.token === _token;
};

AuthBO.hasAccess = () => {
    return (AuthBO.sess.user)?true:false;
};
module.exports = AuthBOFnc;