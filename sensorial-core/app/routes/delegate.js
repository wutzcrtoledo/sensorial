var os = require('os');
var fs = require("fs");
const EventsBo = require("../bo/EventsBO");
const AuthBo = require('../bo/AuthBO');
const UserActBO = require("../bo/UserActBO");
var conf =  JSON.parse(fs.readFileSync("./config/default.json"));

module.exports = function(app, logger) {	
	'use strict';
	 
    app.use(function(req, res, next) {
		console.log("I'm setting the headers");
		//console.log(conf);
		//res.header("Access-Control-Allow-Origin", "http://localhost:3000");
		//res.header("Access-Control-Allow-Origin", "http://sensorial.bluezinc.cl");
		res.header("Access-Control-Allow-Origin", conf.clientAllowed);		
		res.header("Access-Control-Allow-Credentials", true);			
		res.header("Access-Control-Allow-Headers", "X-Requested-With, authorization,content-type");
		res.header("Access-Control-Allow-Methods", "GET,HEAD,PUT,PATCH,POST,DELETE");
		next();
	});	 

	
	app.post("/login", function(req, res) {
		try {
			console.log("I'm setting the headers for LOGIN");			
		//	req.session.cookie.httpOnly = false;
			var authIns = new AuthBo(req.session, logger);
			const evBo = new EventsBo(logger);
			//console.log(req.body);			
			authIns.login(req.body, (_res)=>{
				//console.log(req.session);
				if(_res.logged){
					evBo.getSensorsForUser(_res.user,(_sens)=>{
						req.session.sensors = _sens;
						res.json(_res);
					});
				}
				else
					res.json(_res);
			});
		} catch (err) {
			logger.error(err);
			res.status(500).json({"error": "Internal Error","code":500});
		}		
	});

	app.get("/logout", function(req, res) {
		try {
			var authIns = new AuthBo(req.session, logger);
			authIns.logOut();
			res.json({OK:true});
		} catch (err) {
			logger.error(err);
			res.status(500).json({"error": "Internal Error","code":500});
		}		
	});

	app.get("/refresh", function(req, res) {
		try {
			//var authIns = new AuthBo(req.session, logger);
			//authIns.refreshSession();
			res.json({OK:true});
		} catch (err) {
			logger.error(err);
			res.status(500).json({"error": "Internal Error","code":500});
		}		
	});

	app.post("/changePassword", function(req, res) {
		try {
			var authIns = new AuthBo(req.session, logger);
			//var tokauth = req.get('Authorization'); 
			if(authIns.hasAccess()){
				const usrActBo = new UserActBO(logger);
				usrActBo.updatePassword(req.session.user, req.body.currentPassword, req.body.newPassword, _res => {
					res.json(_res);
				});
			}
			else{
				res.status(401).json({"error": "Not Valid Session","code":401});
			}
		} catch (err) {
			 logger.error(err);
			 res.status(500).json({"error": "Internal Error","code":500});
		}
				
	});
	
	app.get("/getEvents/([0-9]{4}\-[0-9]{2}\-[0-9]{2})|/getEvents$", function(req, res) {	
		try {
			//res.header('Access-Control-Allow-Origin', "");
			console.log("I'm setting the headers for GETEVENTS");
			
		//	req.session.cookie.httpOnly = false;
			const evBo = new EventsBo(logger);
			//var tokauth = req.get('Authorization'); 			
			//console.log(tokauth);
			//console.log(req.get("xhrFields"));
			var authIns = new AuthBo(req.session, logger);
//			console.log(req.session);
			//if(authIns.hasAccess(tokauth)){	
			if(authIns.hasAccess()){	
				var _dateStr = req.url.replace(/\/getEvents[\/]*/ig,"");
				evBo.getAllEventsForDate(req.session.user, req.session.sensors,_dateStr ,_events => {
					//res.setContentType("text/plain");
					res.json({OK:true,items:_events});
				});
			}
			else{
				res.status(401).json({"error": "Not Valid Session","code":401});
			}
		} catch (err) {
			logger.error(err);
			res.status(500).json({"error": "Internal Error","code":500});
		}				
	});

	app.get("/getSensorsForUser", function(req, res) {	
		try {
			const evBo = new EventsBo(logger);
			var authIns = new AuthBo(req.session, logger);
			if(authIns.hasAccess()){
				evBo.getSensorsForUser(req.session.user, _res=>{
					res.json({OK:true,items:_res});
				});
			}
			else{
				res.status(401).json({"error": "Not Valid Session","code":401});
			}
		} catch (err) {
			logger.error(err);
			res.status(500).json({"error": "Internal Error","code":500});
		}				
	});
};
