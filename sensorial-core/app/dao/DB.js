var dbCl = require("./MySQLDBClass");
const Sequelize = require('sequelize'); 

const tbSeDvrDevice = dbCl.sequelize.define('se_dvr_device', {
dvrid: {field: 'dvrid', type: Sequelize.STRING, primaryKey: true},
deviceName: {field: 'device_name', type: Sequelize.STRING, allowNull: true},
deviceEmail: {field: 'device_email', type: Sequelize.STRING, allowNull: true}},
 {freezeTableName: true, timestamps:false});

const tbSeDvrSensor = dbCl.sequelize.define('se_dvr_sensor', {
channelId: {field: 'channel_id', type: Sequelize.INTEGER, primaryKey: true},
dvrid: {field: 'dvrid', type: Sequelize.STRING, primaryKey: true},
sensorName: {field: 'sensor_name', type: Sequelize.STRING, allowNull: true},
sensorType: {field: 'sensor_type', type: Sequelize.STRING, allowNull: true}},
 {freezeTableName: true, timestamps:false});

const tbSeEvents = dbCl.sequelize.define('se_events', {
eventId: {field: 'event_id', type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
channelId: {field: 'channel_id', type: Sequelize.INTEGER},
deviceId: {field: 'device_id', type: Sequelize.STRING},
when: {field: 'when', type: Sequelize.DATE},
channelName: {field: 'channel_name', type: Sequelize.STRING, allowNull: true},
status: {field: 'status', type: Sequelize.STRING, allowNull: true}},
 {freezeTableName: true, timestamps:false});

const tbSeUser = dbCl.sequelize.define('se_user', {
userId: {field: 'user_id', type: Sequelize.STRING, primaryKey: true},
password: {field: 'password', type: Sequelize.STRING},
fullName: {field: 'full_name', type: Sequelize.STRING, allowNull: true},
email: {field: 'email', type: Sequelize.STRING}},
 {freezeTableName: true, timestamps:false});

const tbUserHasDvr = dbCl.sequelize.define('user_has_dvr', {
userId: {field: 'user_id', type: Sequelize.STRING, primaryKey: true},
dvrid: {field: 'dvrid', type: Sequelize.STRING, primaryKey: true}},
 {freezeTableName: true, timestamps:false});

module.exports = {
 tbSeDvrDevice: tbSeDvrDevice,
 tbSeDvrSensor: tbSeDvrSensor,
 tbSeEvents: tbSeEvents,
 tbSeUser: tbSeUser,
 tbUserHasDvr: tbUserHasDvr,
 query : dbCl.getJSONFromQuery,
 seqInst: dbCl.seqInst
};