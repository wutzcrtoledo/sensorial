const Sequelize = require('sequelize');
const fs = require("fs");
var conf =  JSON.parse(fs.readFileSync("./config/default.json"));
var dbAccess = conf.mysql;
const sequelize = new Sequelize(dbAccess.database, dbAccess.user, dbAccess.password, {
  host: dbAccess.host,
  port: (dbAccess.port || 3306),
  dialect: 'mysql',
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
  operatorsAliases: false
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });


var getJSONFromQuery = (query, replacements, callback) => {
  if(typeof replacements === "function"){
      callback = replacements;
      replacements = null;
  }
  if(!replacements){
      sequelize.query(query, { type: sequelize.QueryTypes.SELECT})
      .then(result => {
          callback(result);
      }).error(function(err) {
          callback(err);
      });
  }
  else{
    sequelize.query(query, {replacements:  replacements,type: sequelize.QueryTypes.SELECT})
      .then(result => {
          callback(result);
      }).error(function(err) {
          callback(err);
      });
  }
};



module.exports = {
   sequelize: sequelize,
   getJSONFromQuery : getJSONFromQuery,
   seqInst: sequelize
};
