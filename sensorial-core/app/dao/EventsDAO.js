const db = require("./DB");

const getEventsByDate = (userId, date, callback) => {
  
    var sql = `SELECT  ev.event_id,
                        ev.channel_id,
                        ev.device_id, 
                        DATE_FORMAT(\`when\`, '%Y-%m-%d %H:%i:%s') as \`when\`, 
                        ev.channel_name,
                        ev.status,
                        sen.sensor_name,
                        sen.sensor_type,
                        dev.device_name
                    FROM se_events ev, se_dvr_sensor sen, se_dvr_device dev, user_has_dvr usr
                    WHERE ev.channel_id = sen.channel_id 
                    AND ev.device_id = sen.dvrid 
                    AND sen.dvrid = dev.dvrid
                    AND usr.dvrid = dev.dvrid
                    AND usr.user_id = :user_id
                    AND DATE_FORMAT(\`when\`, '%Y-%m-%d') = :date 
                ORDER BY ev.channel_id asc, ev.when desc`;
                    
    //console.log(sql);
    db.query(sql, { user_id: userId, date: date } ,_res => {
        //console.log(_res);
        callback(_res);
    });
};

const getSensorsForUser = (_userId, callback) =>{
    var sql = `SELECT dev.dvrid, dev.device_name, sen.channel_id, sen.sensor_name 
                FROM user_has_dvr ud, se_dvr_device dev, se_dvr_sensor sen
                WHERE ud.dvrid = dev.dvrid 
                AND sen.dvrid = dev.dvrid
                AND ud.user_id = :user_id`;

            //console.log(sql);
            db.query(sql, { user_id: _userId} ,_res => {
            //console.log(_res);
            callback(_res);
            });
};

module.exports = {
    getEventsByDate: getEventsByDate, 
    getSensorsForUser: getSensorsForUser
};
