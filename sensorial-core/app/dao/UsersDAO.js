const dbCl = require("./DB");


const getUser = (_user, callback) => {
    const {or} = dbCl.seqInst.Op;
    dbCl.tbSeUser.findOne({where: {
        [or]: [{userId: _user.userId}, {email: _user.userId}]        
    }})          
    .then(_userRes => {
            callback(_userRes);
    });
};


module.exports = {
    getUser: getUser
};