var express = require("express");
var fs = require("fs");
var bodyParser = require("body-parser");
var cors = require("cors");
const winston = require('winston');
var logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    new winston.transports.File({ filename: './logs/error.log', level: 'error' }),
    new winston.transports.File({ filename: './logs/info.log', level: 'info' }),
    new winston.transports.Console()
  ]
});
var conf =  JSON.parse(fs.readFileSync("./config/default.json"));
var app = module.exports = express();

const session = require('express-session');
var MySQLStore = require('express-mysql-session')(session);
var sessionStore = new MySQLStore(conf.mysql);
app.use(session({
	key: 'session_cookie_name',
	secret: 'session_cookie_secret',
	store: sessionStore,
	resave: false,
	saveUninitialized: false
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

try {
    // Load all routes
    require('./routes')(app, logger);
} catch (e) {
    logger.error(e.message);
}