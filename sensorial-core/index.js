'use strict';
// stand-alone index.js
var logger = require('winston');
var fs = require('fs');
var app = require('./app/index');
var https = require ('https');

var conf =  JSON.parse(fs.readFileSync("./config/default.json"));
// Read port from command line, config, or default
//logger.info(process.env);
var port = (process.argv[2] || (conf.port || 3000));
if(conf.ssl){  
let keys_dir = {key:"/home/wutzcouk/ssl/keys",
                ca:"/home/wutzcouk/ssl/csrs",
                cert:"/home/wutzcouk/ssl/certs"};
let server_options = { 
  key  : fs.readFileSync(keys_dir.key + '/927ed_66087_4ceb998b2a1ede9fd6a30b74fbbcce5e.key'),
  //ca   : fs.readFileSync(keys_dir + 'certrequest.csr'), 
  cert : fs.readFileSync(keys_dir.cert + '/bluezinc_cl_927ed_66087_1541987332_31aee4d299f89b66a83f3e9c16166e68.crt') 
};    
                                                  
    https.createServer(server_options,app).listen(port, ()=>{
      logger.info('Listening with SSL on port ' + port +'...');
    });
}
else{
  app.listen(port, function () {
    logger.info('Listening on port ' + port +'...');
  });
}
