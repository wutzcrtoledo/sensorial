const mails = require(__dirname+"/../lib/EmailClient");
const _ = require("underscore");
const moment = require("moment-timezone");
const evImporter = require(__dirname+"/../lib/EventsImporter");


const parseLinesToObj = (devices, callback) => {
    let channelRexg = "ID de canal\: [0-9]\,";
  //  let sensorNameRegx = "Nombre\:CAMERA[0-9]+";
    let sensorNameRegx = "{sen}[a-zA-Z0-9\-]+{\/sen}";
    let tiempoRegx = "[0-9]{4}\-[0-9]{1,2}\-[0-9]{1,2} [0-9]{1,2}\:[0-9]{1,2}\:[0-9]{1,2}";
    let locTimezone = moment.tz.guess();
    console.log(locTimezone);
    for(var devName in devices){
        if(devices[devName].length > 0){
            for(var x in devices[devName]){
                let event = devices[devName][x];
                let obj = {
                    channelId : event.match(new RegExp(channelRexg,"ig")),
                    channelName : event.match(new RegExp(sensorNameRegx,"ig")),
                    timestamp : event.match(new RegExp(tiempoRegx,"ig"))
                };
                obj.channelId = obj.channelId[0].replace(/[^0-9]/ig,"");
                obj.channelName = obj.channelName[0].replace(/\{sen\}|\{\/sen\}/ig,"");
		
                obj.timestamp = moment.tz(obj.timestamp[0], "YYYY-MM-DD HH:mm:ss").format();
                //var startStamp = moment.tz(stampString, "D-MMM-YYYY HH:mm A", evTimezone);
               // obj.timestamp = obj.timestamp[0];
                devices[devName][x] = obj;
            }
        }
    }
    callback(devices);
};

const splitLines = (chunkStr, callback) => {
    let line = 1;
    //let deviceIdRegex = "[a-zA-Z\.\_]+@gmail.com";
    let deviceIdRegex = "{dvr}[a-zA-Z\.\_\-]+{\/dvr}";
    //let deviceIdRegex = "From\:\s*\s\<[a-zA-Z\.\_]+@gmail.com\>";
    //let deviceIdRegex = "From: Cristian Toledo <cristiantoledo@gmail.com>";
    let starterStrDevice = "From:";
    let starterStrSensored = "ID de canal:";
    let linesObj = {others: []};
    let devId = null;
   // let chunkStr = "";
        //console.log(chunkStr);
        if(!devId){
            devId = chunkStr.match(new RegExp(deviceIdRegex,"ig"));
        //    console.log(devId?devId[0]||"None DEV Id Found":"No DEV");
            devId = devId?devId[0]:null;
            if(devId)
               devId = devId.replace(/\{dvr\}|\{\/dvr\}/ig,"");
               console.log(devId);
        }
        let chunkLines = chunkStr.toString().split("\n");
        for(var i = 0 ;i < chunkLines.length; i++){
            if(chunkLines[i].startsWith(starterStrSensored)){
                if(devId){
                    if(!linesObj[devId])
                        linesObj[devId] = [];
                    linesObj[devId].push(chunkLines[i] + " - "+chunkLines[i+1]);
                }
                else
                    linesObj.others.push(chunkLines[i] + " - "+chunkLines[i+1]);
            }
       //     console.log(line + " .::::. "+ chunkLines[i]);
           // line++;
        }
        console.log("Finished");
        callback(linesObj);    
};


const processMails = (mailList, callback) => {
    //console.log(mailList.length);
    if(mailList.length > 0){
        let mail2Send = mailList.splice(0,1);
        splitLines(mail2Send[0].body, _res=>{    
            callback(_res);    
            processMails(mailList, callback);       
        });
    }
    else
       callback({finished:true});
};

const init=() => {
        mails.getMailList((_mailList)=>{
            if(_mailList.emptyBox){
                console.log("Nothing To Refresh");
                process.exit();
            }
            //console.log(_mailList);
            let linesObj = {others: []};
            processMails(_mailList, (_res) => {
               if(!_res.finished){
                    for(var x in _res){
                        if(!linesObj[x])
                            linesObj[x] = [];
                        linesObj[x] = _.union(_res[x], linesObj[x]);
                    } 
                }
                else{
                   // console.log(linesObj);
                    
                    parseLinesToObj(linesObj, _formattedRes => {
                     //   console.log(_formattedRes);                                                
                        evImporter.importEvents(_formattedRes, allOK => {
                            console.log(allOK);
                            process.exit();
                        });                        
                    });
                   
                   // console.log("LLAMADA AL CALLBACK FINAL");
                }
            });
        });
};
init();
