const db = require("./db/DB");
const _ = require("underscore");

const importEvents = (evObj, callback) => {
    let ping=0, pong=0, errorlogs = [];
    for(var deviceId in evObj){
        for(var x in evObj[deviceId]){
            let recNode = evObj[deviceId][x];
                let tmpObj = {channelId:parseInt(recNode.channelId)||1,
                              deviceId:deviceId!=="others"?deviceId:"missed_node_dvr",
                              when:recNode.timestamp||null,
                              channelName:recNode.channelName||null};
               // console.log(tmpObj);    
               //records.push(tmpObj);
               ping++;
               db.tbSeEvents.create(tmpObj)
               .then(_res => {
                    pong++;
                  //  errorlogs.push({obj: _.pick(tmpObj, 'channelId', 'when'), OK:true});
                    if(ping === pong) callback({OK:true});
                }).catch(_ex => {  
                     pong++; 
                     console.log(_ex);
                     errorlogs.push({obj: _.pick(tmpObj, 'channelId', 'when'), error: _ex});
                     if(ping === pong) callback(errorlogs);
                });   
        }     
    }   
};


module.exports = {
    importEvents : importEvents
};