var Imap = require('imap'), inspect = require('util').inspect;
const _ = require("underscore");
const fs = require("fs");
const conf =  JSON.parse(fs.readFileSync(__dirname+"/../config/default.json"));

var imap = new Imap(conf.emailAccount);

function openInbox(cb) {
  imap.openBox('INBOX', false, cb);
}

//const packMail = (stream, info, callback) => {
  //  callback({info:info,body:stream});    
//};

const packMail = (stream, seqno ,mailObj, callback) => {
    //const chunks = [];
    stream.on("data", function (chunk) {
      mailObj.body += chunk.toString("utf-8");
    });
    stream.on("end", function () {
        callback(seqno, mailObj);
    });
};

const getMailList = (finalCallback) => {
  try{
        imap.once('ready', function() {
            openInbox(function(err, box) {
                let exMails = {};
                let emailIdList = [];
                if (err) throw err;
                imap.search([ 'UNSEEN', ['FROM' , conf.usrMailList ] ], function(err, results) {
                  if (err) throw err;
                  console.log("Result FOUND" + results.length);
                  if(results.length <= 0){
                    finalCallback({emptyBox:true});
                    return ;
                  }
                  var f = imap.fetch(results, { bodies: ['HEADER.FIELDS (FROM,SUBJECT)','TEXT'] });                  
                  f.on('message', function(msg, seqno) {    
                    console.log('Message #%d', seqno);
                    var prefix = '(#' + seqno + ') ';
                    msg.on('body', function(stream, info) {
                      console.log(prefix + 'Body');
                    //  stream.pipe(fs.createWriteStream('inbox/msg-' + seqno + '-body.txt'));
                    // exMails.push({info:info,file:'msg-' + seqno + '-body.txt'});     
                    console.log(info);     
                    if(!exMails[info.seqno])
                        exMails[info.seqno] = {info: info, body: ""};

                    packMail(stream, info.seqno ,exMails[info.seqno], (_seqno, _mail) =>{
                        exMails[_seqno] = _mail;
                    });
                    });
                    msg.once('attributes', function(attrs) {
                      imap.setFlags(attrs.uid, 'Deleted', ()=>{
                        console.log("Message "+attrs.uid+" Flagged to DElete");
                      }); 
                      console.log(prefix + 'Attributes: %s', inspect(attrs, false, 8));
                    });
                    msg.once('end', function() {
                      console.log(prefix + 'Finished');
                      emailIdList.push(seqno);
                      //console.log(exMails);
                    });
                  });
                  f.once('error', function(err) {
                    console.log('Fetch error: ' + err);
                  });
                  f.once('end', function() {
                    console.log('Done fetching all messages!');
                    //console.log(box.permFlags);
                  // imap.expunge(emailIdList);
                    imap.closeBox(true, ()=>{
                        console.log("Box Closed");
                        imap.end();
                        finalCallback(_.toArray(exMails));
                    });             
                    
                  });
                });
              });
        });
        
        imap.once('error', function(err) {
          console.log(err);
        });
        
        imap.once('end', function() {
          console.log('Connection ended');
        });
        
        imap.connect();
  }
  catch(ex){
    finalCallback({emptyBox:true});
  }     
};

module.exports = {
    getMailList : getMailList
};
//getMailList((_res)=>{console.log("DONE");});
