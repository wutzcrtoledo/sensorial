var dbCl = require("./MySQLDBClass");
const Sequelize = require('sequelize'); 

const tbSeEvents = dbCl.sequelize.define('se_events', {
    eventId: {field: 'event_id', type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    channelId: {field: 'channel_id', type: Sequelize.INTEGER},
    deviceId: {field: 'device_id', type: Sequelize.STRING},
    when: {field: 'when', type: Sequelize.DATE},
    channelName: {field: 'channel_name', type: Sequelize.STRING, allowNull: true},
    status: {field: 'status', type: Sequelize.STRING, allowNull: true}},
{freezeTableName: true, timestamps:false});

 
module.exports = {
 tbSeEvents: tbSeEvents,
 query : dbCl.getJSONFromQuery};